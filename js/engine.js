$(function() {

// Выпадающее меню
$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
    $('.nav.navbar-nav').fadeToggle()
});



// Banner-slider
$('.banner').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    dots: true
});


// News-slider
$('.news-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    arrows: true,
    responsive: [
    {
      breakpoint: 1260,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});


// Стилизация селектов
$('select').styler();










// КАРТА Яндекс


ymaps.ready(init);

function init(){     

    var myMap;

    myMap = new ymaps.Map("YMapsID", {
        center: [67.637525, 52.986096],
        zoom: 16,
        controls: []
    });
    
    myMap.behaviors.disable('scrollZoom');
    
    myMap.controls.add("zoomControl", {
        position: {top: 15, left: 15}
    });
    
    var html  = '<div class="map-info">';
        html +=     '<div class="map-info__text">';
        html +=         '<p>Ненецкий аграрно-экономический <br/> техникум имени В.Г. Волкова</p>';
        html +=     '</div>';
        html += '</div>';
    
    var myPlacemark = new ymaps.Placemark([67.633855, 52.987481],
        {
          balloonContent: html
        },
        { iconLayout: 'default#image',
          // iconImageHref: 'http://blog.karmanov.ws/files/APIYaMaps1/min_marker.png',
          iconImageSize: [40, 51],
          iconImageOffset: [-20, -47],
          balloonLayout: "default#imageWithContent",
          balloonContentSize: [289, 151],
          // balloonImageHref: 'http://blog.karmanov.ws/files/APIYaMaps1/min_popup.png',
          balloonImageOffset: [-144, -147],
          balloonImageSize: [289, 151],
          balloonShadow: false });     

    myMap.geoObjects.add(myPlacemark);
    myPlacemark.balloon.open();
}

})